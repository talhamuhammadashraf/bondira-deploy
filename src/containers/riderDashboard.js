import React ,{Component} from 'react';
import {RiderDashboard,Header} from '../components/'
import {Affix} from 'antd'

class RiderDash extends Component{
    render(){
        return(
            <div
            style={{
                backgroundColor:"#41404c",
                height:"100%"
            }}
            >
            <Affix>            
            <Header/>
            </Affix>            
            <RiderDashboard/>
            </div>
        )
    }
}
export default RiderDash