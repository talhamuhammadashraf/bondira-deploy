import React ,{Component} from 'react'
import {Rating,Header} from '../components'
import {Affix} from 'antd'
class RatingComponent extends Component{
    render(){
        return(
            <div
                style={{
                    backgroundColor: "#41404c",
                    height: "100vh"
                }}
            >
                <Affix>
                    <Header />
                </Affix>
                <br />
                <Rating id={this.props.routeParams.id}/>
            </div>

        )
    }
}
export default RatingComponent