import React ,{Component} from 'react'
import {Header,CreateOrder} from '../components';

class CreateOrderContainer extends Component{
    render(){
        return(
            <div
            style={{
                backgroundColor:"#41404c",
                // height:"100vh"
            }}            >
                <Header/>
                <CreateOrder/>
            </div>
        )
    }
}
export default CreateOrderContainer