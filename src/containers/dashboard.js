import React ,{Component} from 'react';
import {Header} from '../components'
import {browserHistory} from 'react-router'
import {Button,Input,Affix,message} from 'antd'
import styles from '../styles/dashboard.css'
import style from '../App.css'
import firebase from 'firebase'
class Dashboard extends Component{
    constructor(){
        super();
        this.state={
            comments:""
        }
    }
componentDidMount(){
    firebase.auth().onAuthStateChanged((user)=>{
        if(user){
            console.log(user,"this is user",user.uid)
            firebase.database().ref("user").child(user.uid).once("value",(snap)=>
            {console.log(snap.val(),"this sympbol")
            var obj = snap.val();
            obj &&
            obj.businessDetail?
            browserHistory.push(`/${obj.type}`):
            browserHistory.push('/riderInfo ')
            }
           )
            
        }})
}
    render(){
        return(
            <div>
                <Affix>
                <Header/>
                </Affix>
            <div className="App" 
            >
                <div
                style={{
                    height:"100vh",
                    flex:1,
                    flexDirection:"row",
                    flexWrap:"wrap",
                    alignContent:"strech"
                    }}
                >
                <div className="gridDash">
                <Button ghost className="ghost" onClick={()=>browserHistory.push('/loginEmail')}>Sign In With Email</Button>
                <br/>or<br/>
                <a onClick={()=>browserHistory.push('/registerEmail')}>Register</a>
                </div>
                <div className="gridDash">
                <Button ghost className="ghost" onClick={()=>browserHistory.push('/loginPhone')}>Sign In With Phone Number</Button>
                <br/>or<br/>
                <a onClick={()=>browserHistory.push('/registerPhone')}>Sign Up Using Phone</a>
                </div>
                <div className="comments" >
                
                <Input.TextArea onChange={(event)=>{this.setState({comments:event.target.value})}}
                style={{borderRadius:25,backgroundColor:"#626268",color:"white"}}
                rows="7"
                placeholder="Any suggestions or queries"
                value={this.state.comments}
                />
                <br/>
                <br/>
                <Button ghost id="submit"
                onClick={(event)=>{
                    event.preventDefault();
                    firebase.database().ref("suggestions").push(this.state.comments)
                    .then(()=>{this.setState({comments:""});message.success("Thanks for your feedback")})
                }}
                >Submit</Button>                
                </div>
            </div>
            </div>
            </div>
        )
    }
}
export default Dashboard