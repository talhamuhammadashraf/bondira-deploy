import React ,{Component} from 'react'
import {SignUpWithEmail,Header} from '../components'
class RegisterEmail extends Component{
    render(){
        return(
            <div 
            style={{
                backgroundColor:"#41404c",
                height:"100vh"
            }}
            >
                <Header/>
                <br/>
                <SignUpWithEmail/>
            </div>
        )
    }
}
export default RegisterEmail