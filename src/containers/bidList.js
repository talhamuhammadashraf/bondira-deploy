import React ,{Component} from 'react'
import {BidList,Header} from '../components'

class BidListComponent extends Component{
    constructor(props){
        super(props);
    }
    render(){console.log(this.props,"in container bid")
        return(
            <div
            style={{
                backgroundColor:"#41404c",
                minHeight:"100vh"
            }}
            >
            <div
            style={{height:"100%"}}
            >
                <Header/>
                <br/>
                <BidList list={this.props.params.list}/>
            </div>
            </div>
        )
    }
}
export default BidListComponent