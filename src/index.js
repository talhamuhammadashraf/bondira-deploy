import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import firebase from 'firebase'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyB2hCXu_HndpdSrhbEP6XobGT73nyTgJcs",
    authDomain: "react-prac-8e4a5.firebaseapp.com",
    databaseURL: "https://react-prac-8e4a5.firebaseio.com",
    projectId: "react-prac-8e4a5",
    storageBucket: "react-prac-8e4a5.appspot.com",
    messagingSenderId: "650862483596"
  };
  firebase.initializeApp(config);


ReactDOM.render(
  <MuiThemeProvider>
<App />
</MuiThemeProvider>
, document.getElementById('root'));
registerServiceWorker();
