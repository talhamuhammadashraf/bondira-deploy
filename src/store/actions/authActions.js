export default class authActions {
    static signUp = () =>({
        type:"signUp"
    })
    static signUpSuccess = (payload) =>({
        type: "signUpSuccess",
        user:payload
    })
    static signUpFail = (payload) =>({
        type: "signUpFail",
        error:payload
    })
    static signIn = () =>({
        type:"signIn"
    })
    static signInSuccess = (payload) =>({
        type: "signInSuccess",
        user:payload
    })
    static signInFail = (payload) =>({
        type: "signInFail",
        error:payload
    })
    static signInPhone = () =>({
        type:"signIn"
    })
    static signInSuccessPhone = (payload) =>({
        type: "signInSuccess",
        user:payload
    })
    static signInFailPhone = (payload) =>({
        type: "signInFail",
        error:payload
    })
    static signUpPhone = () =>({
        type:"signIn"
    })
    static signUpSuccessPhone = (payload) =>({
        type: "signInSuccess",
        user:payload
    })
    static signUpFailPhone = (payload) =>({
        type: "signInFail",
        error:payload
    })

}