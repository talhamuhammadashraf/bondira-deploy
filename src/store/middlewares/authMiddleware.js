import firebase, { messaging } from 'firebase'
import {browserHistory} from 'react-router'
import authActions from '../actions/authActions'
import {message} from 'antd'

export const signup = (email, pass, user) => {
    console.log(email, pass, user)
    return (dispatch) => {
        dispatch(authActions.signUp())
        firebase.auth().createUserWithEmailAndPassword(email, pass)
            .then((data) => {
                // firebase.auth().currentUser.updateProfile({ displayName: name }).then(() => {
                    firebase.database().ref("user")
                        .child(firebase.auth().currentUser.uid)
                        .set({
                            email :email,
                            uid : firebase.auth().currentUser.uid,
                            type:user,
                            rating:0
                        })
                        dispatch(authActions.signUpSuccess(data))

                // })
                // user == "trucker"? browserHistory.push('/trucker'):browserHistory.push('/riderInfo')
                browserHistory.push('/riderInfo')
            })
            .catch((error) => {
                console.log(error);
                dispatch(authActions.signUpFail(error))
            }
            )

    }
}
export const signin = (email,pass)=>{
console.log(email,pass)
return (dispatch)=>{
     dispatch(authActions.signIn())
     firebase.auth().signInWithEmailAndPassword(email,pass)
     .then((user)=>{
         message.success("Success")
         console.log(user,"222222222222222222")
         firebase.database().ref("user").child(user.uid).child("type").once("value",(snap)=>
         {dispatch(authActions.signInSuccess(user))
         browserHistory.push(`/${snap.val()}`)}
        )
     })
     .catch(
         (error)=>{
             message.error(error.code)
             dispatch(authActions.signInFail(error))
         }
     )
}
}

export const signupPhone=(phone,verify)=>{
    return(dispatch)=>{
        dispatch(authActions.signUpPhone())
        firebase.auth().signInWithPhoneNumber(phone,verify)
    }
}