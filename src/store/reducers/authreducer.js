const INITIAL_STATE = {
isLoading : false,
isLoggedIn:false,
isRegistered:false,
user:{},
error:{}
}

export const authReducer = (state= INITIAL_STATE,action) =>{console.log(action)
switch(action.type){
    case "signUp":
    return {...state,isLoading:true}
    case "signUpSuccess":
    return{...state,isLoading:false,isLoggedIn:true,isRegistered:true,user:action.user}
    case "signUpFail":
    return{...state,isLoading:false,isLoggedIn:false,error:action.error}
    case "signIn":
    return {...state,isLoading:true}
    case "signInSuccess":
    return{...state,isLoading:false,isLoggedIn:true,user:action.user}
    case "signInFail":
    return{...state,isLoading:false,isLoggedIn:false,error:action.error}
    default :
    return state

}
}