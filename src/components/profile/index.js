import React ,{Component} from 'react'
import {Card,Rate, Button, Collapse} from 'antd'
import firebase from 'firebase'
const Panel = Collapse.Panel;
class Profile extends Component{
    constructor(props){
        super(props);
        this.state={}
    }
    componentDidMount(){
        firebase.database().ref("user").child(this.props.id)
        .on("value",(snap)=>{
            this.setState({profile:snap.val()})
            
        })
    }
    render() {
        console.log(this.props,"props check in profile",this.state)
        return (
            <div><center>
                {this.state.profile?
                    <Card style={{backgroundColor:"#675B68",color:"white",width:"80%"}}
                    >
                    <table>
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{this.state.profile.businessDetail.name}</td>
                            </tr>
                            <tr>
                                <td>Business Name</td>
                                <td>{this.state.profile.businessDetail.businessName}</td>
                            </tr>
                            <tr>
                                <td>Business Address</td>
                                <td>{this.state.profile.businessDetail.address}</td>
                            </tr>
                            {this.state.profile.email &&
                            <tr>
                                <td>Email</td>
                                <td>{this.state.profile.email}</td>
                            </tr>}
                            {this.state.profile.businessDetail.phone2 &&
                            <tr>
                                <td>Phone</td>
                                <td>{this.state.profile.businessDetail.phone2}</td>
                            </tr>}
                            <tr>
                                <td>Phone</td>
                                <td>{this.state.profile.businessDetail.phone1}</td>
                            </tr>
                            <tr>
                                <td>Rating</td>
                                <td><Rate disabled defaultValue={this.state.profile.rating} /></td>
                            </tr>
                            <tr>
                                <td>General Information</td>
                                <td>{this.state.profile.businessDetail.generalInformation}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br/>
                    {this.state.profile.comments && 
                    <Collapse>
                    <Panel header="Comments">
                    {this.state.profile.comments.map((data,ind)=>
                    <div key={ind}>
                    <table>
                        <tbody>
                            <tr>
                                <th >{Object.keys(data)}</th>
                                <td  colSpan={3}>{Object.values(data)}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    )}
                    </Panel>
                    </Collapse>
                    }
                    </Card>:
                    <div>Loading</div>
                }</center>
            </div>
        )
    }
}
export default Profile