import React ,{Component} from 'react'
import { Form,Icon, Input, Button,message,Radio, Checkbox } from 'antd';
import styles from '../../styles/signupPhone.css';
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {signupPhone} from '../../store/middlewares/authMiddleware'
import firebase from 'firebase'
import Select from 'antd/lib/select';
const FormItem = Form.Item;
const Option = Select.Option
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const mapStateToProps = () =>({

})


const mapDispatchToProps = (dispatch)=>{console.log(dispatch)
  return bindActionCreators({
    signupPhone
  },dispatch
)
}

class SignUpPhone extends Component {
  constructor(props){
    super(props);
    this.state={visibility:"inline",
    validateStatus:"success",
    help:""
  }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err && this.state.validateStatus === "success") {
        window.credentials = values
        console.log('Received values of form: ', values);
        var phoneNum = values.phone.split("");
        while(phoneNum[0]==0){phoneNum.shift()}
        phoneNum = phoneNum.join("")
        console.log(phoneNum,"phonenum")    
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
          'size': 'normal',
          'callback': function(response) {
            firebase.auth().signInWithPhoneNumber(`+${phoneNum}`,window.recaptchaVerifier).then(
              (confirmationResult)=>{
                window.confirmationResult=confirmationResult;
              console.log(window.confirmationResult,"this is magic")
              browserHistory.push("/verifyCode")  
              }
            )
          },
          'expired-callback': function() {
            alert("please refresh page")
          }
        }) 
  
      window.recaptchaVerifier.render().then(function(widgetId) {
      window.recaptchaWidgetId = widgetId;
  
      });
  
      }
      else{message.error("Error! Please Input correct information")}
    });
  }
  render() {console.log("this.props",this.props)
  
    const { getFieldDecorator } = this.props.form;
    
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <FormItem
          validateStatus={this.state.validateStatus}
          help={this.state.help}

          label="Phone Number"
        >
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: 'Please input your phone number!' }],
          })(
            <Input style={{ width: '100%' }}
              prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Phone Number eg: 0020 123 1234567 "
              type="tel"
              onChange={(event) => {
                event.target.value.match(/^[0-9]+$/) ?
                  this.setState({ validateStatus: "success", help: "" }) :
                  this.setState({ validateStatus: "error", help: "Please enter your phone number eg : 0020 123 1234567" })
              }}
            />
          )}
        </FormItem>
        <FormItem
        >
          {getFieldDecorator('userType'
            , {
              rules: [{ required: true, message: 'Please Select' }],
            })(
              <RadioGroup style={{ color: "white" }}>
                <Radio checked={true} value="trucker"><span style={{ color: "white", fontSize: "1.2em" }}>Trucker</span></Radio>
                <Radio value="rider"><span style={{ color: "white", fontSize: "1.2em" }}>Rider</span></Radio>
              </RadioGroup>
            )}
        </FormItem>
        <FormItem>
          <Button ghost htmlType="submit" className="login-form-button"
            style={{ display: this.state.visiblility }}
          >
            Send code
          </Button>

          <br />
        </FormItem>
        <div id="recaptcha-container"></div>
      </Form>
    );
  }
}

const SignUpWithPhone = Form.create()(SignUpPhone);
export default connect(mapStateToProps,mapDispatchToProps)(SignUpWithPhone)