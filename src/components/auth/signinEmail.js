import React ,{Component} from 'react'
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import styles from '../../styles/signinEmail.css';
import {browserHistory} from 'react-router';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {signin} from '../../store/middlewares/authMiddleware'


const FormItem = Form.Item;

const mapStateToProps = () =>({
  
  })
  
  const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
      signin
    },dispatch
  )
  }
  

class SignInEmail extends React.Component {
  constructor(props){
    super(props);
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.signin(values.email,values.password)
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email!' }],
          })(
            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} type="email" placeholder="Email" 
            style={{color:"white"}}
            />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
            type="password" placeholder="Password" 
            />
          )}
        </FormItem>
        <FormItem>
          <Button ghost htmlType="submit" className="login-form-button">
            Log in
          </Button>
          <br/>
          Or <a 
          onClick={(ev)=>{
            ev.preventDefault();
            browserHistory.push("/registerEmail")
          }}
          >register now!</a>
        </FormItem>
      </Form>
    );
  }
}

const SignInWithEmail = Form.create()(SignInEmail);
export default connect(mapStateToProps,mapDispatchToProps)(SignInWithEmail)