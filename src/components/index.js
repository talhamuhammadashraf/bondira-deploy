import Header from './header'
import SignInWithEmail from './auth/signinEmail'
import SignUpWithEmail from './auth/signupEmail'
import SignUpWithPhone from './auth/signupPhone'
import VerifyCode from './auth/verifyCode'
import RiderBusiness from './rider/businessDetails'
import TruckerDashboard from './trucker/dashboard'
import RiderDashboard from './rider/dashboard'
import CreateOrder from './rider/createOrder'
import Rating from './trucker/rating'
import SignInWithPhone from './auth/signinPhone'
import Profile from './profile'
import BidList from './rider/bidList'
export {    Header,
            SignInWithEmail,
            SignUpWithEmail,
            BidList,
            SignInWithPhone,
            Profile,
            VerifyCode,
            TruckerDashboard,
            SignUpWithPhone,
            RiderDashboard,
            CreateOrder,
            Rating,
            RiderBusiness}