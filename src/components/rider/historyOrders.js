import React ,{Component} from 'react'
import { Card } from 'antd';
import firebase from 'firebase';
import {browserHistory} from 'react-router'
class HistoryOrders extends Component{
    constructor(){
        super();
        this.state={arr:[]}
    }
    componentDidMount(){
        firebase.auth().onAuthStateChanged((user)=>{
            if (user){

                firebase.database().ref().child("orders").orderByChild("riderUID").
                equalTo(user.uid).on("value",(snap)=>{
                    console.log(snap.val())
            var values = snap.val();
            var arr=[];
            for(var i in values){
                arr.push(values[i])
            }
            this.setState({
                arr:arr.reverse()
            })
            console.log(arr,"array")
        })
    }
    else{
        browserHistory.push('/')
    }
})
    }
    render(){
        const arr = this.state.arr.slice(0,20)
        return(
            <div>
                {!arr.length ? 
                <div style={{color:"white",fontStyle:"italic",fontWeight:"bold"}}>Waiting for your first order</div> :
                arr.map((data,index) => 
                <div
                key={index}
                >
                <Card 
                loading={false} 
                bordered={true} 
                hoverable={true}
                style={{
                        backgroundColor:"#655E68",
                        width:"80%",
                        marginRight:"auto",
                        marginLeft:"auto",
                        borderColor:"#272727"
                         }}>
                <div 
                style={{color:"white"}}
                >
                    <h2
                    style={{color:"white"}}
                    >From : {data.from}</h2>
                    <h2
                    style={{color:"white"}}
                    >To : {data.to}</h2>
                    <h3
                    style={{color:"white"}}
                    >{new Date(data.postedAt).toLocaleString()}</h3>
                <ul>
                    <li><b>Must Have</b>{data.mustHave}</li>
                    <li><b>Prefer to  Have</b>{data.preferToHave}</li>
                    <li><b>Status</b>{data.status}</li>
                    
                </ul>
                </div>
                </Card>
                <br/>
                </div> 
            )}
            </div>
        )
    }
}

export default HistoryOrders