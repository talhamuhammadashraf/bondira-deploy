import React ,{Component} from 'react'
import { Form, Input, Tooltip, Icon, Cascader, message, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import firebase from 'firebase'
import {browserHistory} from 'react-router'
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;


class SimpleForm extends React.Component {
    state = {
    };
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
          var pushID = firebase.database().ref().child("orders").push().getKey();
          console.log(pushID,"000000000000000000000")
          firebase.database().ref().child("orders").child(pushID).set({
              currency:values.prefix,
              to:values.to,
              from:values.from,
              cost:values.cost,
              mustHave:values.mustHave,
              preferToHave:values.preferToHave,
              riderUID:firebase.auth().currentUser.uid,
              status:"NEW"  ,
              postedAt:Date.now(),
              orderID : pushID
          },()=>{this.props.form.resetFields();
            message.success("Order posted Successfully");
            browserHistory.push('/')
          })
        }
      });
    }
  
    render() {
      const { getFieldDecorator } = this.props.form;
  
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      };
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 8,
          },
        },
      };
      const prefixSelector = getFieldDecorator('prefix', {
        initialValue: 'E£',
      })(
        <Select style={{ width: 70 }}>
          <Option value="E£">E£</Option>
        </Select>
      );
      return (
        <div
        style={{
          justifyContent:"center"
        }}
        >
        <Form onSubmit={this.handleSubmit} className="business-form">
          <FormItem
            label={(
              <span style={{color:"white"}}>
                To
              </span>
            )}
          >
            {getFieldDecorator('to', {
              rules: [{ required: true, message: 'Please input destination location!', whitespace: true }],
            })(
              <Input 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
                <span style={{color:"white"}}>
                From
                </span>
              )}
          >
            {getFieldDecorator('from', {
              rules: [{ required: true, message: 'Please select your Pickup point' }],
            })
            (
              <Input 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
                <span style={{color:"white"}}>
                Cost
                </span>
              )}
          >
            {getFieldDecorator('cost', {
              rules: [{ required: true, message: 'Amount to be paid' }],
            })(
              <Input addonAfter={prefixSelector} style={{ width: '100%' }}
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
                <span style={{color:"white"}}>
                Must Have
                </span>
              )}
          >
            {getFieldDecorator('mustHave', {
              rules: [{ required: true, message: 'must have' }],
            })(
              <Input style={{ width: '100%' }} 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>          
          <FormItem
            label={(
                <span style={{color:"white"}}>
                Prefer to have
                </span>
              )}
          >
            {getFieldDecorator('preferToHave', {
              rules: [{ required: true, message: 'Prefer to have' }],
            })(
              <Input style={{ width: '100%' }} 

              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
                      label={(
                        <span style={{color:"white"}}>
                        Details
                        </span>
                      )}        
          >
            {getFieldDecorator('details', {
              rules: [{  message: 'Please provide your further details' }],
            })(
              <Input.TextArea
              style={{borderRadius:25,textAlign:"right",backgroundColor:"#626268",color:"white"}}
              rows="7"
              placeholder="Please provide further details"
              />          )}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
          <Button ghost htmlType="submit">Submit</Button>
          </FormItem>
        </Form>
        </div>
      );
    }
  }
  
  
const CreateOrder = Form.create()(SimpleForm);
export default CreateOrder