import React ,{Component} from 'react';
import firebase from 'firebase'
import {Card,Collapse,Rate,message,Button} from 'antd'
import {browserHistory} from 'react-router'
const Panel = Collapse.Panel;
class BidList extends Component{
    constructor(){
        super();
        this.state={
            bids:[],
            awarded:false
        }
    }
    award(uid){
        firebase.database().ref("orders").child(this.props.list).update(
            JSON.parse(JSON.stringify({ 
                "awardedTo": uid,
                "status":"ACCEPTED"
             }))
        ).then(()=>{message.success("Successfully Awarded");browserHistory.push("/rider")})
        .catch(()=>{message.error("Error!")})
    }
    componentDidMount(){
        firebase.database().ref("orders").child(this.props.list).child("acceptedBy").on(
            "value",(snap)=>{
                var biddersArray = snap.val();
                var biddersDetail = [];
                biddersArray && biddersArray.map((value,index)=>{
                    firebase.database().ref("user").child(value).once("value",(snap)=>{
                        biddersDetail.push(snap.val())
                        console.log(biddersDetail,"this is bidders details")
                        this.setState({bids:biddersDetail})
                    })
                })
                console.log(biddersArray,"this is bidders array")
            }
        )
        firebase.database().ref("orders").child(this.props.list).child("awardedTo").on("value",(snap)=>{
            console.log(snap.val(),"awarded @@@@@@@22")
            snap.val() ? this.setState({awarded:true}) : this.setState({awarded:false})
        })

    }
    render(){
        console.log(this.props,"for bidlist")
        console.log(this.state,"state in bid compo")
        return(
            <div><center>
            {this.state.bids.length?this.state.bids.map((data,index)=><div key={index}>
                <Card style={{backgroundColor:"#675B68",color:"white",width:"80%",borderColor:"#222222"}}
                actions={[
                    <Button onClick={()=>this.award(data.uid)}
                    disabled={this.state.awarded}
                    >Award</Button>
                ]}
                >
                <table>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>{data.businessDetail.name}</td>
                        </tr>
                        <tr>
                            <td>Business Name</td>
                            <td>{data.businessDetail.businessName}</td>
                        </tr>
                        <tr>
                            <td>Business Address</td>
                            <td>{data.businessDetail.address}</td>
                        </tr>
                        {data.email &&
                        <tr>
                            <td>Email</td>
                            <td>{data.email}</td>
                        </tr>}
                        {data.businessDetail.phone2 &&
                        <tr>
                            <td>Phone</td>
                            <td>{data.businessDetail.phone2}</td>
                        </tr>}
                        <tr>
                            <td>Phone</td>
                            <td>{data.businessDetail.phone1}</td>
                        </tr>
                        <tr>
                            <td>Rating</td>
                            <td><Rate disabled defaultValue={data.rating} /></td>
                        </tr>
                        <tr>
                            <td>General Information</td>
                            <td>{data.businessDetail.generalInformation}</td>
                        </tr>
                    </tbody>
                </table>
                {data.comments && 
                    <Collapse>
                    <Panel header="Comments">
                    {data.comments.map((data,ind)=>
                    <div key={ind}>
                    <table>
                        <tbody>
                            <tr>
                                <th >{Object.keys(data)}</th>
                                <td  colSpan={3}>{Object.values(data)}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    )}
                    </Panel>
                    </Collapse>
                    }

                </Card>
                <br/>
                </div>):
                <div style={{color:"white",fontSize:"200%"}}>Not Accepted yet</div>
            }</center>
        </div>
        
        )
    }
}
export default BidList