import React ,{Component} from 'react';
import firebase from 'firebase'
import { browserHistory} from 'react-router'
import {Card,Button,Icon, Avatar,Modal ,message} from 'antd'
const confirm = Modal.confirm
class LiveOrders extends Component{
    constructor(){
        super();
        this.state={arr:[]}
    }
    done(orderID,awardedTo){
        confirm({
            title: 'Do you Want to mark DELIVERED ?',
            content: 'Are you sure',
            onOk() {
                firebase.database().ref("orders").child(orderID).update(
                    JSON.parse(JSON.stringify({ 
                        "status":"DONE"
                     }))
                )
                .then(()=>{message.success("Your order has been marked Delivered!");
                browserHistory.push(`/rating/${awardedTo}`)
            }
            )
                .catch(()=>message.error("Error please try again later"))
            },
            onCancel() {
              message.success('Cancelled');
            },
          });

    }
    cancelOrder(orderID){
        confirm({
            title: 'Do you Want to cancel this order?',
            content: 'Are you sure',
            onOk() {
                firebase.database().ref("orders").child(orderID).remove()
                .then(()=>{message.success("Your order has been cancelled!");
            }
            )
                .catch(()=>message.error("Error please try again later"))
            },
            onCancel() {
              message.success('Cancelled');
            },
          });
    }

    componentDidMount(){
        firebase.auth().onAuthStateChanged((user)=>{
            if(user){
                firebase.database().ref().child("orders")
                .orderByChild("riderUID").equalTo(user.uid)
                .on("value",(snap)=>{
                    console.log(snap.val())
                    var values = snap.val();
                    var arr=[];
                    for(var i in values){
                        arr.push(values[i])
                    }
                    this.setState({
                        arr:arr.reverse()
                    })
                    console.log(arr,"array")
                })
            }
        })
    }
    render(){
        return(
            <div>
                {!this.state.arr.length ? 
                <div style={{color:"white"}}><b><i>No orders yet...</i></b></div> :
                    this.state.arr.map((data, index) =>{
                        if(data.status !== "DONE")
                        {return <div
                        key={index}
                    >
                        <Card
                            loading={false}
                            bordered={true}
                            hoverable={true}
                            style={{
                                backgroundColor: "#655E68",
                                width: "80%",
                                marginRight: "auto",
                                marginLeft: "auto",
                                borderColor: "#272727"
                            }}
                            actions={[
                            <Button 
                            disabled={data.status !== "NEW" ? true :false}
                            onClick={()=>this.cancelOrder(data.orderID)}
                            >Cancel</Button>,
                            <Button
                            style={{visibility: data.awardedTo ? "visible" : "hidden"}}
                            onClick={()=>this.done(data.orderID,data.awardedTo)}
                            >Done</Button>,
                            <Button onClick={()=>browserHistory.push(`/bidList/${data.orderID}`)}>Accepted By</Button>
                            ]}
                        >
                            <div
                                style={{ color: "white" }}
                            >
                                <table>
                                    <thead>
                                        <tr>
                                            <th>From:</th>
                                            <th>{data.from}</th>
                                        </tr>
                                        <tr>
                                            <th>To:</th>
                                            <th>{data.to}</th>
                                        </tr>
                                        <tr>
                                            <th colSpan={2}>{new Date(data.postedAt).toLocaleString()}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{data.currency}:</td>
                                            <td><h4 style={{ color: "white" }}><i>{data.cost}</i></h4></td>
                                        </tr>
                                        <tr>
                                            <td>Must Have:</td>
                                            <td>{data.mustHave}</td>
                                        </tr>
                                        <tr>
                                            <td>Prefer to  Have:</td>
                                            <td>{data.preferToHave}</td>
                                        </tr>
                                        <tr>
                                            <td>Status:</td>
                                            <td>{data.status}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </Card>
                        <br />
                    </div>}
                    }
                )}
            </div>

        )
    }
} 
export default LiveOrders