import React from 'react';
import {Affix} from 'antd'
import {Link} from 'react-router'
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import CreateOrder from './createOrder';
import HistoryOrders from './historyOrders'
import LiveOrders from './liveOrders'
import style from '../../styles/tabs.css'
const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 10,
  },
};

export default class RiderDashboard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0,
    };
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
  };

  render() {
    const fontSize = window.innerWidth <600 ? 10 :15
    return (
      <div>
        <Affix offsetTop={108}>        
        <Tabs
          onChange={this.handleChange}
          value={this.state.slideIndex}
        >
          <Tab label="History Orders" value={0} 
                    style={{
                        backgroundColor:"#636068",
                        fontSize:fontSize      

                    }}
          className="tabs"
            />
          <Tab label="Live Orders" value={1}
                    style={{
                        fontSize:fontSize      ,
                        backgroundColor:"#636068"
                    }} 
                    className="tabs"
                    />
          <Tab label="Create Orders" value={2} 
                    style={{
                        backgroundColor:"#636068",
                        fontSize:fontSize      

                    }}
                    className="tabs"
                    />
        </Tabs>
        </Affix>        
        <SwipeableViews
          index={this.state.slideIndex}
          onChangeIndex={this.handleChange}
        >
          <div
          id="history"
          >
            <HistoryOrders/> 
            <br />
          </div>
          <div style={styles.slide}>
          <LiveOrders/>
          </div>
          <div style={styles.slide}>
            <CreateOrder/>
          </div>
        </SwipeableViews>
      </div>
    )
  }
}   