import React ,{Component} from 'react';
import firebase from 'firebase'
import {browserHistory} from 'react-router'
import {Card,Button,Icon, Avatar,Modal} from 'antd'
import Rating from './rating';
const confirm = Modal.confirm

class AcceptedOrder extends Component{
    constructor(){
        super();
        this.state={arr:[]}
    }
    componentWillMount(){
        firebase.auth().onAuthStateChanged((user)=>{
            if(!user){browserHistory.push("/")}
            else{

                
                firebase.database().ref().child("orders").orderByChild("awardedTo")
                .equalTo(firebase.auth().currentUser.uid).on("value",(snap)=>{
                    console.log(snap.val())
                    var values = snap.val();
                    var arr=[];
                    for(var i in values){
                        arr.push(values[i])
                    }
                    this.setState({
                        arr:arr.reverse()
                    })
                    console.log(arr,"array")
                })
            }
        })
            }
            render(){console.log(this.state,"state")
            const arr = this.state.arr.slice(0,1)    
        return(
            <div>
                {!arr.length ? 
                <div style={{color:"white"}}><b><i>No orders yet...</i></b></div> :
                    arr.map((data, index) =>
                    <div
                        key={index}
                    >
                        <Card
                            loading={false}
                            bordered={true}
                            hoverable={true}
                            style={{
                                backgroundColor: "#655E68",
                                width: "80%",
                                marginRight: "auto",
                                marginLeft: "auto",
                                borderColor: "#272727"
                            }}
                            actions={[
                            <Button color="black"
                            style={{visibility:data.ratedByTrucker ? "hidden" : "visible"}}
                            disabled={data.status === "DONE" ? false : true}
                            onClick={()=>{
                                firebase.database().ref("orders").child(data.orderID)
                                .update(
                                    JSON.parse(JSON.stringify({ 
                                        "ratedByTrucker":true
                                     }))
                                ).then(()=>console.log("updated to done"))
                                ;
                                browserHistory.push(`/rating/${data.riderUID}`)}
                            }
                            >Rate</Button>
                        ]}
                        >
                            <div
                                style={{ color: "white" }}
                            >
                                <table>
                                    <thead>
                                        <tr>
                                            <th>From:</th>
                                            <th>{data.from}</th>
                                        </tr>
                                        <tr>
                                            <th>To:</th>
                                            <th>{data.to}</th>
                                        </tr>
                                        <tr>
                                            <th colSpan={2}>{new Date(data.postedAt).toLocaleString()}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{data.currency}:</td>
                                            <td><h4 style={{ color: "white" }}><i>{data.cost}</i></h4></td>
                                        </tr>
                                        <tr>
                                            <td>Must Have:</td>
                                            <td>{data.mustHave}</td>
                                        </tr>
                                        <tr>
                                            <td>Prefer to  Have:</td>
                                            <td>{data.preferToHave}</td>
                                        </tr>
                                        <tr>
                                            <td>Status:</td>
                                            <td>{data.status}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </Card>
                        <br />
                    </div>
                )}
            </div>

        )
    }
} 
export default AcceptedOrder